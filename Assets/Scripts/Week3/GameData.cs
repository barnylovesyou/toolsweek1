using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameData 
{
    public string playerName;
    public int currentLevel;
    public int savePointID;
    public Weapon currentEquippedWeapon;
    public Armour currentEquippedArmour;
    //public Inventory inventory;
    public List<Skills> skills;
}
