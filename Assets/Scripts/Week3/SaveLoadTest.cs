using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class SaveLoadTest : MonoBehaviour
{
    public GameData gd;
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.S))
        {
            BinarySaveDataManager.Save(gd);
        }
        if(Input.GetKeyDown(KeyCode.L))
        {
            gd = BinarySaveDataManager.Load();
        }
    }
}

[CustomEditor(typeof(SaveLoadTest))]
public class SaveAndLoad : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        SaveLoadTest saveUI = (SaveLoadTest)target;
        if(GUILayout.Button("Serialize to Binary Data"))
        {
            BinarySaveDataManager.Save(saveUI.gd);
        }
        if (GUILayout.Button("Serialize to Json Data"))
        {
            SaveDataManager.Save(saveUI.gd);
        }
        if (GUILayout.Button("Load Json Data"))
        {
            SaveLoadTest slt = (SaveLoadTest)target;
            slt.gd =SaveDataManager.Load();
        }
        if (GUILayout.Button("Load Binary Data"))
        {
            SaveLoadTest slt = (SaveLoadTest)target;
            slt.gd = BinarySaveDataManager.Load();
        }
    }
}