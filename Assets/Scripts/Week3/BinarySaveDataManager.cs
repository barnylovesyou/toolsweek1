
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

public class BinarySaveDataManager : MonoBehaviour
{
    public static string directory = "/SaveData/";
    public static string fileName = "SaveData.txt";
    public static void Save(GameData gd)
    {
        string dir = Application.persistentDataPath + directory;

        if (!Directory.Exists(dir))
        {
            Directory.CreateDirectory(dir);
        }
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(dir+fileName);
        bf.Serialize(file, gd);
        file.Close();
        Debug.Log("Saved In " + dir);
    }
    public static GameData Load()
    {
        string fullPath = Application.persistentDataPath + directory + fileName;
        GameData gd = new GameData();

        if (File.Exists(fullPath))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(fullPath, FileMode.Open);
            gd = (GameData)bf.Deserialize(file);
            return gd;
        }
        else
        {
            Debug.Log("Save file does not exist " + fullPath);
        }
        return gd;
    }
}
