using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Skills
{
    public float castTime;
    public string name;
    public float damage;
    public int skillSlot;
}
