using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunTrigger : MonoBehaviour
{
    [SerializeField] private Animator _animator;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("space"))
        {
            _animator.SetBool("Running", true);
        }
        else
        {
            _animator.SetBool("Running", false);
        }
    }
}
